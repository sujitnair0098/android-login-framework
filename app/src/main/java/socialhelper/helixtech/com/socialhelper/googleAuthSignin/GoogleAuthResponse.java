package socialhelper.helixtech.com.socialhelper.googleAuthSignin;

/**
 * Created by Abhisek on 6/21/2016.
 */

public interface GoogleAuthResponse {

    void onGoogleAuthSignIn(GoogleAuthUser user);

    void onGoogleAuthSignInFailed();

    void onGoogleAuthSignOut(boolean isSuccess);
}
