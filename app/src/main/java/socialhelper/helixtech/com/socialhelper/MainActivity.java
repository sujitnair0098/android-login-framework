package socialhelper.helixtech.com.socialhelper;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import socialhelper.helixtech.com.socialhelper.facebookSignIn.FacebookHelper;
import socialhelper.helixtech.com.socialhelper.facebookSignIn.FacebookResponse;
import socialhelper.helixtech.com.socialhelper.facebookSignIn.FacebookUser;
import socialhelper.helixtech.com.socialhelper.googleAuthSignin.GoogleAuthResponse;
import socialhelper.helixtech.com.socialhelper.googleAuthSignin.GoogleAuthUser;
import socialhelper.helixtech.com.socialhelper.googleAuthSignin.GoogleSignInHelper;
import socialhelper.helixtech.com.socialhelper.twitterSignIn.TwitterHelper;
import socialhelper.helixtech.com.socialhelper.twitterSignIn.TwitterResponse;
import socialhelper.helixtech.com.socialhelper.twitterSignIn.TwitterUser;


public class MainActivity extends AppCompatActivity implements View.OnClickListener,GoogleAuthResponse,FacebookResponse,TwitterResponse {


    private GoogleSignInHelper mGAuthHelper;
    private FacebookHelper mFbHelper;
    private TwitterHelper mTwitterHelper;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //fb api initialization
        mFbHelper = new FacebookHelper(this,
                "id,name,email,gender,birthday,picture,cover",
                this);

        mTwitterHelper = new TwitterHelper(R.string.twitter_api_key,
                R.string.twitter_secrate_key,
                this,
                this);

        mGAuthHelper = new GoogleSignInHelper(this, null, this);
        findViewById(R.id.g_login_btn).setOnClickListener(this);
        findViewById(R.id.g_logout_btn).setOnClickListener(this);
        findViewById(R.id.bt_act_login_fb).setOnClickListener(this);
        findViewById(R.id.bt_act_logout_fb).setOnClickListener(this);
        findViewById(R.id.twitter_login_button).setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.g_login_btn:
                mGAuthHelper.performSignIn(this);
                break;
            case R.id.g_logout_btn:
                mGAuthHelper.performSignOut();
                break;
            case R.id.bt_act_login_fb:
                mFbHelper.performSignIn(this);
                break;
            case R.id.bt_act_logout_fb:
                mFbHelper.performSignOut();
                break;
            case R.id.twitter_login_button:
                mTwitterHelper.performSignIn();
                break;
        }
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        //handle results
        mGAuthHelper.onActivityResult(requestCode, resultCode, data);
        mFbHelper.onActivityResult(requestCode, resultCode, data);
        mTwitterHelper.onActivityResult(requestCode, resultCode, data);
    }
    @Override
    public void onFbSignInFail() {
        Toast.makeText(this, "Facebook sign in failed.", Toast.LENGTH_SHORT).show();
    }
    @Override
    public void onFbSignInSuccess() {
        Toast.makeText(this, "Facebook sign in success", Toast.LENGTH_SHORT).show();
    }
    @Override
    public void onFbProfileReceived(FacebookUser facebookUser) {
        Toast.makeText(this, "Facebook user data: name= " + facebookUser.name + " email= " + facebookUser.email, Toast.LENGTH_SHORT).show();

        Log.d("Person name: ", facebookUser.name + "");
        Log.d("Person gender: ", facebookUser.gender + "");
        Log.d("Person email: ", facebookUser.email + "");
        Log.d("Person image: ", facebookUser.facebookID + "");
    }
    @Override
    public void onFBSignOut() {
        Toast.makeText(this, "Facebook sign out success", Toast.LENGTH_SHORT).show();
    }
    @Override
    public void onGoogleAuthSignIn(GoogleAuthUser user) {
        Toast.makeText(this, "Google user data: name= " + user.name + " email= " + user.email, Toast.LENGTH_SHORT).show();
    }
    @Override
    public void onGoogleAuthSignInFailed() {
        Toast.makeText(this, "Google sign in failed.", Toast.LENGTH_SHORT).show();
    }
    @Override
    public void onGoogleAuthSignOut(boolean isSuccess) {
        Toast.makeText(this, isSuccess ? "Sign out success" : "Sign out failed", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onTwitterError() {
        Toast.makeText(this, "Twitter sign in failed.", Toast.LENGTH_SHORT).show();

    }

    @Override
    public void onTwitterSignIn(@NonNull String userId, @NonNull String userName) {
        Toast.makeText(this, " User id: " + userId + "\n user name" + userName, Toast.LENGTH_SHORT).show();

    }

    @Override
    public void onTwitterProfileReceived(TwitterUser user) {
        Toast.makeText(this, "Twitter user data: name= " + user.name + " email= " + user.email, Toast.LENGTH_SHORT).show();
    }
}
